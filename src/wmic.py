import os
import sys
import subprocess
import json
from pathlib import Path

if os.name != "nt":
    raise NotImplementedError("The wmic tool currently only supports Windows-based operating systems!")

#TODO: Sort alphabetically
with open(Path(__file__).parent / "commands.json") as f:
    _queries = json.load(f)

def _get_indices(string: str):
    return (i for i, c in enumerate(string) if c.isupper())

class Wmic:
    def __init__(self, command: str):
        self.command = command
        self.command_output = subprocess.check_output(f"wmic {self.command} get {','.join(_queries[self.command])}").decode('unicode_escape')
        self.rows = self.command_output.readlines()
        self.titles = self.rows[0]
        self.data = []
        for index, row in enumerate(self.rows[1:]):
            row_data = []
            for position in _get_indices(self.titles):
                if row[position].strip() == "":
                    row_data.append(None)
                else:
                    row_data.append(row.split()[index])
            self.data.append(row_data)

Wmic("timezone")
