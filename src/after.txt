<html>
  <H3>Node: LARSKA-PC - 1 Instances of Win32_BaseBoard</H3>
  <table border="1">
    <tr style="background-color:#a0a0ff;font:10pt Tahoma;font-weight:bold;" align="left">
      <td colspan="2">Base Board<span style="height:1px;overflow-y:hidden">.</span></td>
    </tr>
    <tr style="background-color:#c0c0c0;font:8pt Tahoma;">
      <td>Property Name</td>
      <td>Value</td>
    </tr>
    <tr style="background-color:#f0f0f0;font:10pt Tahoma;">
      <td>Manufacturer</td>
      <td>ASUSTeK COMPUTER INC.<span style="height:1px;overflow-y:hidden">.</span></td>
    </tr>
    <tr style="background-color:#e0f0f0;font:10pt Tahoma;">
      <td>Model</td>
      <td><span style="height:1px;overflow-y:hidden">.</span></td>
    </tr>
    <tr style="background-color:#f0f0f0;font:10pt Tahoma;">
      <td>Name</td>
      <td>Base Board<span style="height:1px;overflow-y:hidden">.</span></td>
    </tr>
    <tr style="background-color:#e0f0f0;font:10pt Tahoma;">
     <td>PartNumber</td>
     <td><span style="height:1px;overflow-y:hidden">.</span></td>
    </tr>
    <tr style="background-color:#f0f0f0;font:10pt Tahoma;">
      <td>PoweredOn</td>
      <td>TRUE<span style="height:1px;overflow-y:hidden">.</span></td>
    </tr>
    <tr style="background-color:#e0f0f0;font:10pt Tahoma;">
      <td>SerialNumber</td>
      <td>140121149600427<span style="height:1px;overflow-y:hidden">.</span></td>
    </tr>
    <tr style="background-color:#f0f0f0;font:10pt Tahoma;">
      <td>SlotLayout</td>
      <td><span style="height:1px;overflow-y:hidden">.</span></td>
    </tr>
    <tr style="background-color:#ffffff;font:10pt Tahoma;font-weight:bold;">
      <td colspan="2"><span style="height:1px;overflow-y:hidden">.</span></td>
    </tr>
  </table>
</html>